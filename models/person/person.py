import random

from .elements.address import Address
from .elements.date_of_birth import DateOfBirth
from .elements.gender import Gender
from .elements.name import Name
from .elements.ssn import Ssn
from faker import Faker


class Person:
    @classmethod
    def generate_random(cls):
        fake = Faker('no_NO')

        gender = Gender.generate_random()
        name = Name.generate_norwegian_name(gender)
        date_of_birth = DateOfBirth.generate_random(100)
        ssn = Ssn.generate_random_ssn(gender, date_of_birth)
        nationality = "Norwegian"
        address = Address.generate_norwegian_random()
        address_secondary = Address.generate_norwegian_random()
        address_tertiary = Address.generate_norwegian_random()
        phone_number = fake.phone_number().replace(" ", "")
        phone_number_secondary = fake.phone_number().replace(" ", "")
        phone_number_tertiary = fake.phone_number().replace(" ", "")
        email = random.choice(
            (name.first_name,
             name.last_name + str(date_of_birth.year),
             name.first_name + name.last_name)
        ).lower() + '@example.com'
        email_secondary = random.choice(
            (name.first_name,
             name.last_name + str(date_of_birth.year),
             name.first_name + name.last_name)
        ).lower() + '@gmail.com'
        email_tertiary = random.choice(
            (name.first_name,
             name.last_name + str(date_of_birth.year),
             name.first_name + name.last_name)
        ).lower() + '@yahoo.com'

        id_type = random.choice(('passport', 'driverslicense', 'nationalidcard'))

        return Person(ssn,
                      name.first_name,
                      name.last_name,
                      date_of_birth,
                      gender,
                      nationality,
                      address,
                      address_secondary,
                      address_tertiary,
                      phone_number,
                      phone_number_secondary,
                      phone_number_tertiary,
                      email,
                      email_secondary,
                      email_tertiary,
                      id_type
                      )

    def __init__(self, ssn, first_name, last_name, date_of_birth, gender,
                 nationality, address, address_secondary, address_tertiary, phone_number, phone_number_secondary, phone_number_tertiary, email, email_secondary, email_tertiary, id_type):
        self.ssn = ssn
        self.first_name = first_name
        self.last_name = last_name
        self.date_of_birth = date_of_birth
        self.gender = gender
        self.nationality = nationality
        self.address = address
        self.address_secondary = address_secondary
        self.address_tertiary = address_tertiary
        self.phone_number = phone_number
        self.phone_number_secondary = phone_number_secondary
        self.phone_number_tertiary = phone_number_tertiary
        self.email = email
        self.email_secondary = email_secondary
        self.email_tertiary = email_tertiary
        self.id_type = id_type

    def to_json(self):
        return {
            'ssn': self.ssn,
            'firstName': self.first_name,
            'lastName': self.last_name,
            'dateOfBirth': self.date_of_birth.to_string(),
            'gender': self.gender.value,
            'nationality': self.nationality,
            'address': {
                'street': self.address.street,
                'postalCode': self.address.postal_code,
                'city': self.address.city,
                'country': self.address.country,
            },
            'addressSecondary': {
                'street': self.address_secondary.street,
                'postalCode': self.address_secondary.postal_code,
                'city': self.address_secondary.city,
                'country': self.address_secondary.country,
            },
            'addressTertiary': {
                'street': self.address_tertiary.street,
                'postalCode': self.address_tertiary.postal_code,
                'city': self.address_tertiary.city,
                'country': self.address_tertiary.country,
            },
            'phoneNumber': self.phone_number,
            'phoneNumberSecondary': self.phone_number_secondary,
            'phoneNumberTertiary': self.phone_number_tertiary,
            'email': self.email,
            'emailSecondary': self.email_secondary,
            'emailTertiary': self.email_tertiary,
            'idType': self.id_type,
        }
